const LOADERS = [
	{
		test: /\.jsx?$/,
		ext: ['.js', '.jsx'],
		install: ['babel-core', 'babel-loader'],
		loader: 'babel-loader',
	},
	{
		test: /\.styl/,
		ext: '.styl',
		install: ['css-loader', 'style-loader', 'stylus', 'stylus-loader'],
		loader: 'style-loader!css-loader!stylus-loader',
	},
	{
		test: /\.tsx?/,
		ext: ['.ts', '.tsx'],
		install: ['awesome-typescript-loader', 'typescript'],
		loader: 'awesome-typescript-loader',
	}
]

module.exports = LOADERS
