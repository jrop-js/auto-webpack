#!/usr/bin/env node
const globby = require('globby')
const {inspect} = require('util')
const MemoryFS = require('memory-fs')
const merge = require('webpack-merge')
const path = require('path')
const prettier = require('prettier')
const webpack = require('webpack')
const yargs = require('yargs')
const _ = require('lodash')

const LOADERS = require('./loaders')
const js = x => inspect(x, null, null)

function stats(src, config = {}) {
	const fs = new MemoryFS()
	const compiler = webpack(
		merge(
			{
				profile: true,
				entry: src,
				output: {
					path: `/tmp`,
					filename: 'output.js',
				},
			},
			config
		)
	)
	compiler.outputFileSystem = fs
	return new Promise((y, n) =>
		compiler.run((err, stats) => {
			if (err) return n(err)
			y(stats.toJson())
		})
	)
}

function deps(src) {
	return stats(src).then(stats => {
		const [chunk] = stats.chunks.concat({modules: [{identifier: src}]})
		return chunk.modules.map(m => m.identifier)
	})
}

function depExts(src) {
	return deps(src).then(deps =>
		_.chain(deps)
			.map(d => path.parse(d).ext)
			.uniq()
			.filter(Boolean)
			.value()
	)
}

async function analyze(...globs) {
	const srcs = await globby(globs)
	let conf = {}
	const loaders = []
	const install = new Set(['webpack', 'webpack-configify'])

	for (const src of srcs) {
		const _stats = await stats(src)
		;(await depExts(src)).forEach(ext => {
			const [loader] = LOADERS.filter(l => l.test.test(ext))
			loaders.push(loader)
			loader.install.forEach(i => install.add(i))
		})
	}

	const prettierConfig = await prettier.resolveConfig(
		path.join(process.cwd(), 'package.json')
	)
	const webpackConfig = prettier.format(
		`
		// npm install --save-dev ${Array.from(install).join(' ')}

		const builder = require('webpack-configify').default
		const prod = process.env.NODE_ENV == 'production'

		const config = builder()
			.production(prod)
			.development(!prod)
			.src(${globs.map(g => `'${g}'`).join(',')})
			.dest('dist')
			${loaders.map(l => `.loader(${js(l.ext)}, ${js(l.loader)})`).join('\n')}
			.merge({
				// your custom config here
			})
			.build()
		module.exports = config

		if (require.main === module) {
			const {inspect} = require('util')
			console.log(inspect(config, null, null))
		}
		`,
		prettierConfig
	)
	console.log(webpackConfig)
}

const args = yargs
	.help('help')
	.alias('h', 'help')
	.version(require('./package').version, 'version')
	.alias('v', 'version').argv
analyze(...args._)
